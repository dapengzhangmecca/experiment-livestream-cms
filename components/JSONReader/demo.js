export default JSON.stringify({
  allShows: [
    {
      showId: 'WStLzwbzIdJeCfSKg0fq',
      tags: ['makeup', 'trending'],
      startTime: '2020-11-27T12:00:00+1100',
      endTime: '2020-11-27T13:00:00+1100',
      title: 'Talking tanning with Elle Effect founder, Elle Ferguson',
      url: 'https://www.mecca.com.au/livestream/',
      location: 'https://www.mecca.com.au/livestream/',
      description:
        'The perfect golden glow: we all want it, and now it’s time to learn how to achieve it with Australian entrepreneur, style influencer and tan-obsessive Elle Ferguson. She’s the creator of cult-followed tanning brand Elle Effect—famous for its velvety tanning mousse—and will be joining us to chat through all her best tips and tricks to achieving a natural-looking, bronze glow.',
      subDescription:
        'Chat along with the MECCA community, ask us questions, and shop as you watch.',
      galleryImages: [
        {
          url:
            'https://res.cloudinary.com/mecca/image/upload/v1605669525/mecca-flagship/livestream-library/01-livestream-tanning_j6p85u.jpg',
          alt: 'Talking tanning with Elle Effect founder, Elle Ferguson',
          title: 'Talking tanning with Elle Effect founder, Elle Ferguson',
          breakpoint: 'default',
        },
      ],
      eventCarouselImages: [
        {
          url:
            'https://res.cloudinary.com/mecca/image/upload/v1605669611/mecca-flagship/events-carousel/01-events-tanning_jkr9nz.jpg',
          alt: 'Talking tanning with Elle Effect founder, Elle Ferguson',
          title: 'Talking tanning with Elle Effect founder, Elle Ferguson',
          breakpoint: 'default',
        },
      ],
      bannerImages: [],
    },
    {
      showId: 'Q535ntaKzhVtaD9FhclI',
      tags: [],
      startTime: '2020-11-27T14:00:00+1100',
      endTime: '2020-11-27T15:00:00+1100',
      title: 'Hairstyling how-to with EdwardsAndCo',
      url: 'https://www.mecca.com.au/livestream/',
      location: 'https://www.mecca.com.au/livestream/',
      description:
        'We’ll sit down with Style Director Matt Jones, and Master Colourist and Stylist Dane, from one of Australia’s leading hair agencies, EdwardsAndCo. With eight salons across the country, each one is renowned for creating creamy blondes, caramel tips, rich brunette colours and on-trend hairstyles. EdwardsAndCo has developed a signature look of sun-kissed highlights and glossy mermaid waves that’s led to thousands of followers on Instagram. Join us to chat through the hair services EdwardsAndCo will have on offer at our new flagship store—with plenty of hair advice along the way.',
      subDescription:
        'Chat along with the MECCA community, ask us questions, and shop as you watch.',
      galleryImages: [
        {
          url:
            'https://res.cloudinary.com/mecca/image/upload/v1605669525/mecca-flagship/livestream-library/02-livestream-hair_gim4zz.jpg',
          alt: 'Hairstyling how-to with EdwardsAndCo',
          title: 'Hairstyling how-to with EdwardsAndCo',
          breakpoint: 'default',
        },
      ],
      eventCarouselImages: [
        {
          url:
            'https://res.cloudinary.com/mecca/image/upload/v1605669612/mecca-flagship/events-carousel/02-events-hair_a91zsn.jpg',
          alt: 'Hairstyling how-to with EdwardsAndCo',
          title: 'Hairstyling how-to with EdwardsAndCo',
          breakpoint: 'default',
        },
      ],
      bannerImages: [],
    },
    {
      showId: '3uqRSw96QGTpy4QzPCX5',
      tags: [],
      startTime: '2020-11-27T16:00:00+1100',
      endTime: '2020-11-27T17:00:00+1100',
      title: 'Jewellery trends & styling with Sarah & Sebastian',
      url: 'https://www.mecca.com.au/livestream/',
      location: 'https://www.mecca.com.au/livestream/',
      description:
        'During this session, beauty and jewellery will intertwine as we meet Sarah Gittoes—the co-founder of luxury jewellers Sarah & Sebastian. Each handcrafted piece by the brand brings an understated elegance and modern style. They use traditional goldsmithing techniques to create intricately designed, limited-edition collections each season. At our new flagship store, Sarah & Sebastian will be offering ear piercing and soldering services; we’ll hear all about that, plus more about the brand and how to style your jewellery and beauty looks with ease.',
      subDescription:
        'Chat along with the MECCA community, ask us questions, and shop as you watch.',
      galleryImages: [
        {
          url:
            'https://res.cloudinary.com/mecca/image/upload/v1605669525/mecca-flagship/livestream-library/03-livestream-jewellery_v1uz2d.jpg',
          alt: 'Jewellery trends & styling with Sarah & Sebastian',
          title: 'Jewellery trends & styling with Sarah & Sebastian',
          breakpoint: 'default',
        },
      ],
      eventCarouselImages: [
        {
          url:
            'https://res.cloudinary.com/mecca/image/upload/v1605669613/mecca-flagship/events-carousel/03-events-jewellery_nnm3fd.jpg',
          alt: 'Jewellery trends & styling with Sarah & Sebastian',
          title: 'Jewellery trends & styling with Sarah & Sebastian',
          breakpoint: 'default',
        },
      ],
      bannerImages: [],
    },
    {
      showId: 'OndamKtnai8ynFnIkX5b',
      tags: [],
      startTime: '2020-11-28T12:00:00+1100',
      endTime: '2020-11-28T13:00:00+1100',
      title: 'MECCA fragrance masterclass',
      url: 'https://www.mecca.com.au/livestream/',
      location: 'https://www.mecca.com.au/livestream/',
      description:
        'Held in our largest ever sensory Perfumeria, this class will have you ready to pick your new signature scent in no time. One of our fragrance experts will chat through the incredible brands we have on offer at our new flagship store, as well as the best way to navigate the range and choose the best scent for you. From understanding fragrance notes to selecting a scent based on how you want to feel when you wear it—this educational masterclass is not to be missed. ',
      subDescription:
        'Chat along with the MECCA community, ask us questions, and shop as you watch.',
      galleryImages: [
        {
          url:
            'https://res.cloudinary.com/mecca/image/upload/v1605669525/mecca-flagship/livestream-library/04-livestream-fragrance_xjrurc.jpg',
          alt: 'MECCA fragrance masterclass',
          title: 'MECCA fragrance masterclass',
          breakpoint: 'default',
        },
      ],
      eventCarouselImages: [
        {
          url:
            'https://res.cloudinary.com/mecca/image/upload/v1605669613/mecca-flagship/events-carousel/04-events-fragrance_g6qs6o.jpg',
          alt: 'MECCA fragrance masterclass',
          title: 'MECCA fragrance masterclass',
          breakpoint: 'default',
        },
      ],
      bannerImages: [],
    },
    {
      showId: '5s4Qo14s1DuuEhsjirda',
      tags: [],
      startTime: '2020-11-28T14:00:00+1100',
      endTime: '2020-11-28T15:00:00+1100',
      title: 'MECCA brow masterclass',
      url: 'https://www.mecca.com.au/livestream/',
      location: 'https://www.mecca.com.au/livestream/',
      description:
        'The angles of the face are determined by the brows; they can lift the eyes, narrow or widen the face, they help to frame and highlight your features and create more symmetry. So, we think it’s only appropriate to host this masterclass with one of our in-store brow experts and hear all about how we can better look after our brows. There’ll be a brow lamination demo; you’ll hear more about the brow services happening at our new flagship store —plus, plenty of tips on how you can show your brows some TLC at home, too.  ',
      subDescription:
        'Chat along with the MECCA community, ask us questions, and shop as you watch.',
      galleryImages: [
        {
          url:
            'https://res.cloudinary.com/mecca/image/upload/v1605669525/mecca-flagship/livestream-library/05-livestream-brow_gedd45.jpg',
          alt: 'MECCA brow masterclass',
          title: 'MECCA brow masterclass',
          breakpoint: 'default',
        },
      ],
      eventCarouselImages: [
        {
          url:
            'https://res.cloudinary.com/mecca/image/upload/v1605669613/mecca-flagship/events-carousel/05-events-brow_doykn5.jpg',
          alt: 'MECCA brow masterclass',
          title: 'MECCA brow masterclass',
          breakpoint: 'default',
        },
      ],
      bannerImages: [],
    },
    {
      showId: 'd8XSdjNegq2iRIk8za77',
      tags: [],
      startTime: '2020-11-29T12:00:00+1100',
      endTime: '2020-11-29T13:00:00+1100',
      title: 'MECCA X Kosas masterclass ',
      url: 'https://www.mecca.com.au/livestream/',
      location: 'https://www.mecca.com.au/livestream/',
      description:
        'Join Kosas makeup artist Josh Collier and learn how to expertly apply a natural, ‘no-makeup makeup’ look using a selection of Kosas bestsellers. During this masterclass you’ll be able to get up close and personal with the brand’s new Revealer Super Creamy + Brightening Concealers and The Sun Show Moisturising Baked Bronzer; plus, their cult favourite lip and face oils. If you are interested in clean beauty that your skin will love, then make sure you tune in for this.',
      subDescription:
        'Chat along with the MECCA community, ask us questions, and shop as you watch.',
      galleryImages: [
        {
          url:
            'https://res.cloudinary.com/mecca/image/upload/v1605669525/mecca-flagship/livestream-library/06-livestream-kosas_imj7kl.jpg',
          alt: 'MECCA X Kosas masterclass ',
          title: 'MECCA X Kosas masterclass ',
          breakpoint: 'default',
        },
      ],
      eventCarouselImages: [
        {
          url:
            'https://res.cloudinary.com/mecca/image/upload/v1605669613/mecca-flagship/events-carousel/06-events-kosas_xkgsrb.jpg',
          alt: 'MECCA X Kosas masterclass ',
          title: 'MECCA X Kosas masterclass ',
          breakpoint: 'default',
        },
      ],
      bannerImages: [],
    },
    {
      showId: 'ahdbeLgb74AluUQW1Yob',
      tags: [],
      startTime: '2020-11-29T14:00:00+1100',
      endTime: '2020-11-29T15:00:00+1100',
      title: 'MECCA skin rituals workshop',
      url: 'https://www.mecca.com.au/livestream/',
      location: 'https://www.mecca.com.au/livestream/',
      description:
        'The calming, self-nourishing and focused nature of applying skincare in the mornings and evenings can do wonders for your mental health, self-esteem and overall productivity. In this workshop we’ll take a closer look at how you can transform your skincare routine into a more mindful ritual with some help from an in-store skin expert. That’s not all, you’ll learn more about the in-store facial services happening at our new flagship store as well as a masterclass showcasing relaxing facial massage techniques you can try at home to relieve tense muscles and help skin appear firmer. ',
      subDescription:
        'Chat along with the MECCA community, ask us questions, and shop as you watch.',
      galleryImages: [
        {
          url:
            'https://res.cloudinary.com/mecca/image/upload/v1605669526/mecca-flagship/livestream-library/07-livestream-skin_ijmyfg.jpg',
          alt: 'MECCA skin rituals workshop',
          title: 'MECCA skin rituals workshop',
          breakpoint: 'default',
        },
      ],
      eventCarouselImages: [
        {
          url:
            'https://res.cloudinary.com/mecca/image/upload/v1605669613/mecca-flagship/events-carousel/07-events-skin_fohwps.jpg',
          alt: 'MECCA skin rituals workshop',
          title: 'MECCA skin rituals workshop',
          breakpoint: 'default',
        },
      ],
      bannerImages: [],
    },
    {
      showId: 'Yk8VzHFkAMHK2CpvqzG2',
      tags: [],
      startTime: '2020-08-13T19:00:00+1000',
      endTime: '2020-08-13T19:45:00+1000',
      title: 'Beauty Election Winners Look',
      url: 'https://www.mecca.com.au/livestream/',
      location: 'https://www.mecca.com.au/livestream/',
      description:
        'Join us live from the MECCA Beauty Lab as our team members Cleo and Courtney take you through MECCA’s most loved products, as voted by you. Watch as they take you through a winning skincare and makeup routine using this year’s Beauty Election winners. ',
      subDescription: '',
      galleryImages: [
        {
          url:
            'https://res.cloudinary.com/mecca/image/upload/v1597817202/live-shopping/2020-08-19/BE_WinnersLook_vaxm84.jpg',
          alt: 'Beauty Election Livestream',
          title: 'Beauty Election Winners Look',
          breakpoint: 'default',
        },
      ],
      eventCarouselImages: [
        {
          url:
            'https://res.cloudinary.com/mecca/image/upload/v1597817202/live-shopping/2020-08-19/BE_WinnersLook_vaxm84.jpg',
          alt: 'Beauty Election Livestream',
          title: 'Beauty Election Winners Look',
          breakpoint: 'default',
        },
      ],
      bannerImages: [
        {
          url:
            'https://res.cloudinary.com/mecca/image/upload/t_mobile/v1596432920/live-shopping/2008-Main-Image-Aug6_1440x1440_dyqpg6.jpg',
          alt: 'Beauty Election Livestream',
          title: 'Beauty Election Winners Look',
          breakpoint: 'default',
        },
        {
          url:
            'https://res.cloudinary.com/mecca/image/upload/t_tablet/v1596432920/live-shopping/2008-Main-Image-Aug6_1440x1440_dyqpg6.jpg',
          breakpoint: 'tablet',
        },
        {
          url:
            'https://res.cloudinary.com/mecca/image/upload/t_laptop/v1596432920/live-shopping/2008-Main-Image-Aug6_1440x1440_dyqpg6.jpg',
          breakpoint: 'laptop',
        },
        {
          url:
            'https://res.cloudinary.com/mecca/image/upload/t_desktop/v1596432920/live-shopping/2008-Main-Image-Aug6_1440x1440_dyqpg6.jpg',
          breakpoint: 'desktop',
        },
        {
          url:
            'https://res.cloudinary.com/mecca/image/upload/t_desktop_large/v1596432920/live-shopping/2008-Main-Image-Aug6_1440x1440_dyqpg6.jpg',
          breakpoint: 'xl',
        },
      ],
    },
    {
      showId: 'I83Qt4AY7GBRN1QjHDzC',
      tags: [],
      startTime: '2020-08-20T20:00:00+1000',
      endTime: '2020-08-20T20:25:00+1000',
      title: 'Signature at-home facial',
      url: 'https://www.mecca.com.au/livestream/',
      location: 'https://www.mecca.com.au/livestream/',
      description:
        'Give your skin the love it deserves with some MECCA-approved me-time. Our skin specialists Courtney and Cleo will be streaming live from the Beauty Lab to guide you through an easy yet effective home facial complete with their must-have products and a NuFace device demo.',
      subDescription: '',
      galleryImages: [
        {
          url:
            'https://res.cloudinary.com/mecca/image/upload/v1598251211/live-shopping/2020-08-19/bl.signature-facial-1_usqtov.jpg',
          alt: 'Beauty Election Livestream',
          title: 'Signature at-home facial',
          breakpoint: 'default',
        },
      ],
      eventCarouselImages: [
        {
          url:
            'https://res.cloudinary.com/mecca/image/upload/v1598251211/live-shopping/2020-08-19/bl.signature-facial-1_usqtov.jpg',
          alt: 'Beauty Election Livestream',
          title: 'Signature at-home facial',
          breakpoint: 'default',
        },
      ],
      bannerImages: [
        {
          url:
            'https://res.cloudinary.com/mecca/image/upload/t_tablet/v1597818030/live-shopping/2020-08-19/image.BeautyElectionEvent_tncm1s.jpg',
          alt: 'Beauty Election Livestream',
          title: 'Signature at-home facial',
          breakpoint: 'default',
        },
        {
          url:
            'https://res.cloudinary.com/mecca/image/upload/t_desktop/v1597818030/live-shopping/2020-08-19/image.BeautyElectionEvent_tncm1s.jpg',
          breakpoint: 'tablet',
        },
      ],
    },
    {
      showId: 'zHzh37PzIxJairzu7D3P',
      tags: [],
      date: 'Thursday 27 August',
      startTime: '2020-08-27T19:00:00+1000',
      endTime: '2020-08-27T19:30:00+1000',
      url: 'https://www.mecca.com.au/livestream/',
      location: 'https://www.mecca.com.au/livestream/',
      title: 'Unboxing September Newness',
      description:
        "Join us live from the Beauty Lab as we unbox September's exciting drop of new-in beauty. Our MECCA specialists will showcase it all including new body care, the latest makeup products and even new brand launches.",
      subDescription:
        'Shop directly from the livestream, chat along with the MECCA community and ask questions in real-time.',
      galleryImages: [
        {
          url:
            'https://res.cloudinary.com/mecca/image/upload/v1600405513/live-shopping/2020-08-27/27Aug2020-Screenshot_copy_v25evo.jpg',
          alt: 'Beauty Election Livestream',
          title: 'Unboxing September Newness',
          breakpoint: 'default',
        },
      ],
      eventCarouselImages: [
        {
          url:
            'https://res.cloudinary.com/mecca/image/upload/v1600405513/live-shopping/2020-08-27/27Aug2020-Screenshot_copy_v25evo.jpg',
          alt: 'Beauty Election Livestream',
          title: 'Unboxing September Newness',
          breakpoint: 'default',
        },
      ],
      bannerImages: [
        {
          url:
            'https://res.cloudinary.com/mecca/image/upload/t_tablet/v1598251110/live-shopping/2020-08-27/livestream-september-newness_ca2gtr.jpg',
          alt: 'Beauty Election Livestream',
          title: 'Unboxing September Newness',
          breakpoint: 'default',
        },
        {
          url:
            'https://res.cloudinary.com/mecca/image/upload/t_desktop/v1598251110/live-shopping/2020-08-27/livestream-september-newness_ca2gtr.jpg',
          breakpoint: 'tablet',
        },
        {
          url:
            'https://res.cloudinary.com/mecca/image/upload/v1598572521/live-shopping/2020-08-27/image.hero_livestream_1280_px_x_1000_uonwu9.jpg',
          breakpoint: 'xxl',
        },
      ],
    },
    {
      showId: 'faQYC0fDn9ESJ9JGkXTs',
      tags: [],
      startTime: '2020-09-03T19:00:00+1000',
      endTime: '2020-09-03T19:30:00+1000',
      url: 'https://www.mecca.com.au/livestream/',
      location: 'https://www.mecca.com.au/livestream/',
      title: 'Natural Beauty 101',
      description:
        'We get it—navigating the natural (or ‘clean’) makeup category can be overwhelming. Live from our Beauty Lab, our hosts will bring our most loved natural brands to life with a step-by-step makeup look as they talk ingredients, formulas and brand values.',
      subDescription:
        'Shop directly from the livestream, chat along with the MECCA community, and ask us questions in real-time.',
      galleryImages: [
        {
          url:
            'https://res.cloudinary.com/mecca/image/upload/v1600405467/live-shopping/2020-09-10/3Sept2020-Screenshot_copy_yceog7.jpg',
          alt: 'Beauty Election Livestream',
          title: 'Natural Beauty 101',
          breakpoint: 'default',
        },
      ],
      eventCarouselImages: [
        {
          url:
            'https://res.cloudinary.com/mecca/image/upload/v1600405467/live-shopping/2020-09-10/3Sept2020-Screenshot_copy_yceog7.jpg',
          alt: 'Beauty Election Livestream',
          title: 'Natural Beauty 101',
          breakpoint: 'default',
        },
      ],
      bannerImages: [
        {
          url:
            'https://res.cloudinary.com/mecca/image/upload/t_tablet/v1597818030/live-shopping/2020-08-19/image.BeautyElectionEvent_tncm1s.jpg',
          alt: 'Beauty Election Livestream',
          title: 'Natural Beauty 101',
          breakpoint: 'default',
        },
        {
          url:
            'https://res.cloudinary.com/mecca/image/upload/t_desktop/v1597818030/live-shopping/2020-08-19/image.BeautyElectionEvent_tncm1s.jpg',
          breakpoint: 'tablet',
        },
        {
          url:
            'https://res.cloudinary.com/mecca/image/upload/v1598847024/live-shopping/2020-09-03/image.banner_1280by900_1_ije4as.jpg',
          breakpoint: 'xxl',
        },
      ],
    },
    {
      showId: 't4bQ57zbkA8YKPPnraLg',
      tags: [],
      startTime: '2020-09-09T19:00:00+1000',
      endTime: '2020-09-09T19:30:00+1000',
      url: 'https://www.mecca.com.au/livestream/',
      location: 'https://www.mecca.com.au/livestream/',
      title: 'How to layer skincare with Gemma Watts',
      description:
        'Layering skincare correctly is an artform. To ensure your skin sings all day long and that you never waste a drop of product, join us live as Editor of Glow Journal and friend of MECCA, Gemma Watts, shares her top tips to take your routine to the next level - morning, noon and night.',
      subDescription:
        'Shop directly from the livestream, chat along with the MECCA community, and ask Gemma questions in real-time.',
      galleryImages: [
        {
          url:
            'https://res.cloudinary.com/mecca/image/upload/v1600403977/live-shopping/2020-09-10/9Sept2020-Screenshot_hvqafv.jpg',
          alt: 'Beauty Election Livestream',
          title: 'How to layer skincare with Gemma Watts',
          breakpoint: 'default',
        },
      ],
      eventCarouselImages: [
        {
          url:
            'https://res.cloudinary.com/mecca/image/upload/v1600403977/live-shopping/2020-09-10/9Sept2020-Screenshot_hvqafv.jpg',
          alt: 'Beauty Election Livestream',
          title: 'How to layer skincare with Gemma Watts',
          breakpoint: 'default',
        },
      ],
      bannerImages: [
        {
          url:
            'https://res.cloudinary.com/mecca/image/upload/t_tablet/v1599453002/live-shopping/2020-09-09/Desktop_Event_1_xyq6l0.jpg',
          alt: 'Beauty Election Livestream',
          title: 'How to layer skincare with Gemma Watts',
          breakpoint: 'default',
        },
        {
          url:
            'https://res.cloudinary.com/mecca/image/upload/t_desktop/v1599453002/live-shopping/2020-09-09/Desktop_Event_1_xyq6l0.jpg',
          breakpoint: 'tablet',
        },
        {
          url:
            'https://res.cloudinary.com/mecca/image/upload/v1599453002/live-shopping/2020-09-09/WideDesktop_Event_1_uqvsga.jpg',
          breakpoint: 'xxl',
        },
      ],
    },
    {
      showId: 'CXfidLrPEhQA8oFPeCRu',
      tags: [],
      startTime: '2020-09-10T19:00:00+1000',
      endTime: '2020-09-10T19:30:00+1000',
      url: 'https://www.mecca.com.au/livestream/',
      location: 'https://www.mecca.com.au/livestream/',
      title: 'Perfecting the At-Home Blow Wave',
      description:
        'Effortless waves should be just that: effortless. To learn how to achieve an at-home blowout—the kind that would rival any salon—join us live from our Beauty Lab and our team will give you the skills (and tools!) to nail perfectly tousles waves at home.',
      subDescription:
        'Shop directly from the livestream, chat along with the MECCA community, and ask us questions in real-time.',
      galleryImages: [
        {
          url:
            'https://res.cloudinary.com/mecca/image/upload/v1599801005/live-shopping/2020-09-17/snip_xoyspb.png',
          alt: 'Beauty Election Livestream',
          title: 'Perfecting the At-Home Blow Wave',
          breakpoint: 'default',
        },
      ],
      eventCarouselImages: [
        {
          url:
            'https://res.cloudinary.com/mecca/image/upload/v1598251211/live-shopping/2020-08-19/bl.signature-facial-1_usqtov.jpg',
          alt: 'Beauty Election Livestream',
          title: 'Perfecting the At-Home Blow Wave',
          breakpoint: 'default',
        },
      ],
      bannerImages: [
        {
          url:
            'https://res.cloudinary.com/mecca/image/upload/t_tablet/v1599200364/live-shopping/2020-09-10/Desktop_Hair_ylkmu0.jpg',
          alt: 'Beauty Election Livestream',
          title: 'Perfecting the At-Home Blow Wave',
          breakpoint: 'default',
        },
        {
          url:
            'https://res.cloudinary.com/mecca/image/upload/t_desktop/v1599200364/live-shopping/2020-09-10/Desktop_Hair_ylkmu0.jpg',
          breakpoint: 'tablet',
        },
        {
          url:
            'https://res.cloudinary.com/mecca/image/upload/v1599200365/live-shopping/2020-09-10/WideDesktop_Hair_cdqq7z.jpg',
          breakpoint: 'xxl',
        },
      ],
    },
    {
      showId: 'jl7iezdcvvb5fG762F5g',
      tags: [],
      startTime: '2020-09-17T19:00:00+1000',
      endTime: '2020-09-17T19:30:00+1000',
      url: 'https://www.mecca.com.au/livestream/',
      location: 'https://www.mecca.com.au/livestream/',
      title: 'Your Acne Answers',
      description:
        'Whether it’s a hormonal flare-up, stubborn blemish or a case of ‘maskne’, we’re no stranger to a mean breakout. If you’re looking for answers—or the perfect acne-fighting routine—this livestream from our Beauty Lab is your chance to find and shop your skin solutions with the help of our skincare experts.',
      subDescription:
        'Shop directly from the livestream, chat along with the MECCA community, and ask us questions in real-time.',
      galleryImages: [
        {
          url:
            'https://res.cloudinary.com/mecca/image/upload/v1600404224/live-shopping/2020-09-24/17Sept2020-Screenshot_copy_ztapde.jpg',
          alt: 'Beauty Election Livestream',
          title: 'Your Acne Answers',
          breakpoint: 'default',
        },
      ],
      eventCarouselImages: [
        {
          url:
            'https://res.cloudinary.com/mecca/image/upload/v1600230057/live-shopping/2020-09-17/carousel-clear_skin_a3hwuw.jpg',
          alt: 'Beauty Election Livestream',
          title: 'Your Acne Answers',
          breakpoint: 'default',
        },
      ],
      bannerImages: [
        {
          url:
            'https://res.cloudinary.com/mecca/image/upload/t_tablet/v1600055368/live-shopping/2020-09-17/clearskin_square_vpk9hb.jpg',
          alt: 'Beauty Election Livestream',
          title: 'Your Acne Answers',
          breakpoint: 'default',
        },
        {
          url:
            'https://res.cloudinary.com/mecca/image/upload/t_desktop/v1600055368/live-shopping/2020-09-17/clearskin_square_vpk9hb.jpg',
          breakpoint: 'tablet',
        },
        {
          url:
            'https://res.cloudinary.com/mecca/image/upload/v1600055369/live-shopping/2020-09-17/clearskin_4by3_b84d0c.jpg',
          breakpoint: 'xxl',
        },
      ],
    },
    {
      showId: 'VYDRAptrZgpKrHG24QDG',
      tags: [],
      startTime: '2020-09-24T19:00:00+1000',
      endTime: '2020-09-24T19:30:00+1000',
      url: 'https://www.mecca.com.au/livestream/',
      location: 'https://www.mecca.com.au/livestream/',
      title: 'The Brow Basics',
      description:
        'Take your brow game to the next level with our top face-framing secrets for full, feathered and fluffy brows. Live from the Beauty Lab, our brow experts will be demonstrating their tried and tested brow shaping tips, while sharing the must-have products in their kits.',
      subDescription:
        'Shop directly from the livestream, chat along with the MECCA community, and ask us questions in real-time.',
      galleryImages: [
        {
          url:
            'https://res.cloudinary.com/mecca/image/upload/v1601007150/live-shopping/2020-09-24/brows_nqz1sa.jpg',
          alt: 'Beauty Election Livestream',
          title: 'The Brow Basics',
          breakpoint: 'default',
        },
      ],
      eventCarouselImages: [
        {
          url:
            'https://res.cloudinary.com/mecca/image/upload/v1601007150/live-shopping/2020-09-24/brows_nqz1sa.jpg',
          alt: 'Beauty Election Livestream',
          title: 'The Brow Basics',
          breakpoint: 'default',
        },
      ],
      bannerImages: [
        {
          url:
            'https://res.cloudinary.com/mecca/image/upload/t_tablet/v1600404258/live-shopping/2020-09-24/Perfect_Brows_720x720px_en8ppo.jpg',
          alt: 'Beauty Election Livestream',
          title: 'The Brow Basics',
          breakpoint: 'default',
        },
        {
          url:
            'https://res.cloudinary.com/mecca/image/upload/t_desktop/v1600404258/live-shopping/2020-09-24/Perfect_Brows_720x720px_en8ppo.jpg',
          breakpoint: 'tablet',
        },
        {
          url:
            'https://res.cloudinary.com/mecca/image/upload/v1600404261/live-shopping/2020-09-24/Perfect_Brows_1280x900px_ircqji.jpg',
          breakpoint: 'xxl',
        },
      ],
    },
    {
      showId: 'GSIN3H47adSonA4EizC4',
      tags: [],
      startTime: '2020-10-01T19:00:00+1000',
      endTime: '2020-10-01T19:30:00+1000',
      title: 'Unboxing October newness',
      url: 'https://www.mecca.com.au/livestream/',
      location: 'https://www.mecca.com.au/livestream/',
      description:
        'Join us live from the Beauty Lab as we unbox October’s extremely juicy drop of brand new beauty. Our MECCA specialists will showcase all the glimmering goods, including new skincare, the latest (and greatest) makeup, and exciting new brand launches.',
      subDescription: '',
      galleryImages: [
        {
          url:
            'https://res.cloudinary.com/mecca/image/upload/v1602113509/live-shopping/2020-10-01/Capture_wmlbyn.jpg',
          alt: 'October newness livestream',
          title: 'Unboxing October newness',
          breakpoint: 'default',
        },
      ],
      eventCarouselImages: [
        {
          url:
            'https://res.cloudinary.com/mecca/image/upload/v1602113509/live-shopping/2020-10-01/Capture_wmlbyn.jpg',
          alt: 'October newness livestream',
          title: 'Unboxing October newness',
          breakpoint: 'default',
        },
      ],
      bannerImages: [
        {
          url:
            'https://res.cloudinary.com/mecca/image/upload/t_mobile/v1601005834/live-shopping/2020-10-01/HERO_IMAGE_DESKTOP_pbyxeb.jpg',
          alt: 'October newness livestream',
          title: 'Unboxing October newness',
          breakpoint: 'default',
        },
        {
          url:
            'https://res.cloudinary.com/mecca/image/upload/t_tablet/v1601005834/live-shopping/2020-10-01/HERO_IMAGE_DESKTOP_pbyxeb.jpg',
          breakpoint: 'tablet',
        },
        {
          url:
            'https://res.cloudinary.com/mecca/image/upload/t_laptop/v1601005834/live-shopping/2020-10-01/HERO_IMAGE_DESKTOP_pbyxeb.jpg',
          breakpoint: 'laptop',
        },
        {
          url:
            'https://res.cloudinary.com/mecca/image/upload/t_desktop/v1601005834/live-shopping/2020-10-01/HERO_IMAGE_DESKTOP_pbyxeb.jpg',
          breakpoint: 'desktop',
        },
        {
          url:
            'https://res.cloudinary.com/mecca/image/upload/t_desktop_large/v1601005834/live-shopping/2020-10-01/HERO_IMAGE_WIDE_DESKTOP_kytnvf.jpg',
          breakpoint: 'xl',
        },
      ],
    },
    {
      showId: 'wbnqZH8xdbewccsV0Hcb',
      tags: [],
      startTime: '2020-10-08T19:00:00+1100',
      endTime: '2020-10-08T19:30:00+1100',
      title: 'New season M.A.C makeup look with Sammy Robinson',
      url: 'https://www.mecca.com.au/livestream/',
      location: 'https://www.mecca.com.au/livestream/',
      description:
        'If there’s ever a time to overhaul your makeup bag and hit refresh on your signature look it’s Spring. Join us live as social media star, Sammy Robinson, talks through a step-by-step makeup look using her favourite M.A.C products and see how she works this new seasons makeup mood.',
      subDescription:
        'Shop directly from the livestream, chat along with the MECCA community and ask questions in real-time.',
      galleryImages: [
        {
          url:
            'https://res.cloudinary.com/mecca/image/upload/v1602803135/live-shopping/2020-10-08/thumb_2_mbyu6w.jpg',
          alt: 'New season M.A.C look with Sammy Robinson',
          title: 'New season M.A.C look with Sammy Robinson',
          breakpoint: 'default',
        },
      ],
      eventCarouselImages: [
        {
          url:
            'https://res.cloudinary.com/mecca/image/upload/v1601619909/live-shopping/2020-10-08/event-carousel_eylmsx.jpg',
          alt: 'New season M.A.C look with Sammy Robinson',
          title: 'New season M.A.C look with Sammy Robinson',
          breakpoint: 'default',
        },
      ],
      bannerImages: [
        {
          url:
            'https://res.cloudinary.com/mecca/image/upload/t_mobile/v1601619909/live-shopping/2020-10-08/desktop_nzd8xz.jpg',
          alt: 'New season M.A.C look with Sammy Robinson ',
          title: 'New season M.A.C look with Sammy Robinson',
          breakpoint: 'default',
        },
        {
          url:
            'https://res.cloudinary.com/mecca/image/upload/t_tablet/v1601619909/live-shopping/2020-10-08/desktop_nzd8xz.jpg',
          breakpoint: 'tablet',
        },
        {
          url:
            'https://res.cloudinary.com/mecca/image/upload/t_laptop/v1601619909/live-shopping/2020-10-08/desktop_nzd8xz.jpg',
          breakpoint: 'laptop',
        },
        {
          url:
            'https://res.cloudinary.com/mecca/image/upload/t_desktop/v1601619909/live-shopping/2020-10-08/desktop_nzd8xz.jpg',
          breakpoint: 'desktop',
        },
        {
          url:
            'https://res.cloudinary.com/mecca/image/upload/t_desktop_large/v1601619910/live-shopping/2020-10-08/wide-desktop_jziqn8.jpg',
          breakpoint: 'xl',
        },
      ],
    },
    {
      showId: 'jXBgKnXf2bJKPuOvW7lh',
      tags: [],
      startTime: '2020-10-15T19:00:00+1100',
      endTime: '2020-10-15T19:30:00+1100',
      title: 'Everyday glam with Too Faced',
      url: 'https://www.mecca.com.au/livestream/',
      location: 'https://www.mecca.com.au/livestream/',
      description:
        "We're putting Too Faced’s new Born This Way Matte Foundation on show in a step-by-step everyday matte glam tutorial. It’s the perfect look for busy bodies wanting to stay flawless from morning to night, with little to no touch-ups throughout the day.",
      subDescription:
        'Shop directly from the livestream, chat along with the MECCA community and ask questions in real-time.',
      galleryImages: [
        {
          url:
            'https://res.cloudinary.com/mecca/image/upload/v1602801478/live-shopping/2020-10-15/toofaced-thumb_catrza.png',
          alt: 'Everyday glam with Too Faced',
          title: 'Everyday glam with Too Faced',
          breakpoint: 'default',
        },
      ],
      eventCarouselImages: [
        {
          url:
            'https://res.cloudinary.com/mecca/image/upload/v1602134841/live-shopping/2020-10-15/everydayglam_3by4_2_x7qpyd.jpg',
          alt: 'Everyday glam with Too Faced',
          title: 'Everyday glam with Too Faced',
          breakpoint: 'default',
        },
      ],
      bannerImages: [
        {
          url:
            'https://res.cloudinary.com/mecca/image/upload/t_mobile/v1602134846/live-shopping/2020-10-15/Everyday_Glam_IMAGE_DESKTOP_r2xein.jpg',
          alt: 'Everyday glam with Too Faced',
          title: 'Everyday glam with Too Faced',
          breakpoint: 'default',
        },
        {
          url:
            'https://res.cloudinary.com/mecca/image/upload/t_tablet/v1602134846/live-shopping/2020-10-15/Everyday_Glam_IMAGE_DESKTOP_r2xein.jpg',
          breakpoint: 'tablet',
        },
        {
          url:
            'https://res.cloudinary.com/mecca/image/upload/t_laptop/v1602134846/live-shopping/2020-10-15/Everyday_Glam_IMAGE_DESKTOP_r2xein.jpg',
          breakpoint: 'laptop',
        },
        {
          url:
            'https://res.cloudinary.com/mecca/image/upload/t_desktop/v1602134846/live-shopping/2020-10-15/Everyday_Glam_IMAGE_DESKTOP_r2xein.jpg',
          breakpoint: 'desktop',
        },
        {
          url:
            'https://res.cloudinary.com/mecca/image/upload/t_desktop_large/v1604293141/live-shopping/2020.11.05/tom-ford-1280x900_p1hvlh.jpg',
          breakpoint: 'xl',
        },
      ],
    },
    {
      showId: 'viIFCFVOhwPHDOWl2NyP',
      tags: [],
      startTime: '2020-10-26T20:00:00+1100',
      endTime: '2020-10-26T20:45:00+1100',
      title: 'Holiday Exclusive Preview Event',
      url: 'https://www.mecca.com.au/livestream/',
      location: 'https://www.mecca.com.au/livestream/',
      description:
        "The wait is over: join us live from the Beauty Lab to be the first to see and shop an edit of Holiday 2020's most exclusive (and unreleased) limited-edition launches. ",
      subDescription:
        'Chat along with the MECCA community, ask us questions live, and shop your favourites before anyone else.',
      galleryImages: [
        {
          url:
            'https://res.cloudinary.com/mecca/image/upload/v1604443402/live-shopping/2020.10.26/thumb_twufwt.png',
          alt: 'Holiday Exclusive Preview Event',
          title: 'Holiday Exclusive Preview Event',
          breakpoint: 'default',
        },
      ],
      eventCarouselImages: [
        {
          url:
            'https://res.cloudinary.com/mecca/image/upload/v1602802035/live-shopping/2020.10.26/holiday_450x600px_dixmsk.jpg',
          alt: 'Holiday Exclusive Preview Event',
          title: 'Holiday Exclusive Preview Event',
          breakpoint: 'default',
        },
      ],
      bannerImages: [
        {
          url:
            'https://res.cloudinary.com/mecca/image/upload/t_mobile/v1602802049/live-shopping/2020.10.26/holiday_720x720px_gatj4u.jpg',
          alt: 'Holiday Exclusive Preview Event',
          title: 'Holiday Exclusive Preview Event',
          breakpoint: 'default',
        },
        {
          url:
            'https://res.cloudinary.com/mecca/image/upload/t_tablet/v1602802049/live-shopping/2020.10.26/holiday_720x720px_gatj4u.jpg',
          breakpoint: 'tablet',
        },
        {
          url:
            'https://res.cloudinary.com/mecca/image/upload/t_laptop/v1602802049/live-shopping/2020.10.26/holiday_720x720px_gatj4u.jpg',
          breakpoint: 'laptop',
        },
        {
          url:
            'https://res.cloudinary.com/mecca/image/upload/t_desktop/v1602802049/live-shopping/2020.10.26/holiday_720x720px_gatj4u.jpg',
          breakpoint: 'desktop',
        },
        {
          url:
            'https://res.cloudinary.com/mecca/image/upload/t_desktop_large/v1602802040/live-shopping/2020.10.26/holiday_1280x900px_nqdadf.jpg',
          breakpoint: 'xl',
        },
      ],
    },
    {
      showId: 'Tnwk1R6JNDwlaESU2aO4',
      tags: [],
      startTime: '2020-11-05T19:00:00+1100',
      endTime: '2020-11-05T19:30:00+1100',
      title: 'A night with Tom Ford Beauty',
      url: 'https://www.mecca.com.au/livestream/',
      location: 'https://www.mecca.com.au/livestream/',
      description:
        'Introducing you to our latest, globally coveted new brand, Tom Ford, with the ultimate high glamour look and showcase of iconic products. Completely interactive, we will talk and swatch while you chat and shop.',
      subDescription: '',
      galleryImages: [
        {
          url:
            'https://res.cloudinary.com/mecca/image/upload/v1604897059/live-shopping/2020.11.05/tom-thumb_jbsyx4.png',
          alt: 'A night with Tom Ford Beauty',
          title: 'A night with Tom Ford Beauty',
          breakpoint: 'default',
        },
      ],
      eventCarouselImages: [
        {
          url:
            'https://res.cloudinary.com/mecca/image/upload/v1604293051/live-shopping/2020.11.05/tom-ford-450x600_qtqa9u.jpg',
          alt: 'A night with Tom Ford Beauty',
          title: 'A night with Tom Ford Beauty',
          breakpoint: 'default',
        },
      ],
      bannerImages: [
        {
          url:
            'https://res.cloudinary.com/mecca/image/upload/t_mobile/v1604292899/live-shopping/2020.11.05/tom-ford-720x720_buwl5w.jpg',
          alt: 'A night with Tom Ford Beauty',
          title: 'A night with Tom Ford Beauty',
          breakpoint: 'default',
        },
        {
          url:
            'https://res.cloudinary.com/mecca/image/upload/t_tablet/v1604292899/live-shopping/2020.11.05/tom-ford-720x720_buwl5w.jpg',
          breakpoint: 'tablet',
        },
        {
          url:
            'https://res.cloudinary.com/mecca/image/upload/t_laptop/v1604292899/live-shopping/2020.11.05/tom-ford-720x720_buwl5w.jpg',
          breakpoint: 'laptop',
        },
        {
          url:
            'https://res.cloudinary.com/mecca/image/upload/t_desktop/v1604292899/live-shopping/2020.11.05/tom-ford-720x720_buwl5w.jpg',
          breakpoint: 'desktop',
        },
        {
          url:
            'https://res.cloudinary.com/mecca/image/upload/t_desktop_large/v1604293141/live-shopping/2020.11.05/tom-ford-1280x900_p1hvlh.jpg',
          breakpoint: 'xl',
        },
      ],
    },
    {
      showId: 'e7JNgqmHeaZcDMwJQgdG',
      tags: [],
      startTime: '2020-11-12T20:00:00+1100',
      endTime: '2020-11-12T20:30:00+1100',
      title: 'From skin prep to skin glow with Maxine Wylde',
      url: 'https://www.mecca.com.au/livestream/',
      location: 'https://www.mecca.com.au/livestream/',
      description:
        'Need a routine refresh that won’t break the bank? This live is dedicated to prepping your skin for the perfect glowy fresh finish using some of MECCA’s most in demand skincare and makeup brands. Join ever-stylish influencer, Maxine Wylde, as she shares her enviable beauty routine live.',
      subDescription:
        'Completely interactive, Maxine will talk and swatch while you chat and shop.',
      galleryImages: [
        {
          url:
            'https://res.cloudinary.com/mecca/image/upload/v1605231568/live-shopping/2020.11.12/maxine-thumb_siozeo.jpg',
          alt: 'Skin glow with Maxine Wylde',
          title: 'Skin glow with Maxine Wylde',
          breakpoint: 'default',
        },
      ],
      eventCarouselImages: [
        {
          url:
            'https://res.cloudinary.com/mecca/image/upload/v1604897156/live-shopping/2020.11.12/maxine-450x600_bfb8qv.jpg',
          alt: 'Skin glow with Maxine Wylde',
          title: 'Skin glow with Maxine Wylde',
          breakpoint: 'default',
        },
      ],
      bannerImages: [
        {
          url:
            'https://res.cloudinary.com/mecca/image/upload/t_mobile/v1604897160/live-shopping/2020.11.12/maxine-720x720_ofnb02.jpg',
          alt: 'Skin glow with Maxine Wylde',
          title: 'Skin glow with Maxine Wylde',
          breakpoint: 'default',
        },
        {
          url:
            'https://res.cloudinary.com/mecca/image/upload/t_tablet/v1604897160/live-shopping/2020.11.12/maxine-720x720_ofnb02.jpg',
          breakpoint: 'tablet',
        },
        {
          url:
            'https://res.cloudinary.com/mecca/image/upload/t_laptop/v1604897160/live-shopping/2020.11.12/maxine-720x720_ofnb02.jpg',
          breakpoint: 'laptop',
        },
        {
          url:
            'https://res.cloudinary.com/mecca/image/upload/t_desktop/v1604897160/live-shopping/2020.11.12/maxine-720x720_ofnb02.jpg',
          breakpoint: 'desktop',
        },
        {
          url:
            'https://res.cloudinary.com/mecca/image/upload/t_desktop_large/v1604897146/live-shopping/2020.11.12/maxine-1280x900_n7fr77.jpg',
          breakpoint: 'xl',
        },
      ],
    },
    {
      showId: 'mghFhSLB1L9e3gJHGcfP',
      tags: [],
      startTime: '2020-11-19T19:30:00+1100',
      endTime: '2020-11-19T20:00:00+1100',
      title: 'Dance Proof Beauty',
      url: 'https://www.mecca.com.au/livestream/',
      location: 'https://www.mecca.com.au/livestream/',
      description:
        "In this step-by-step tutorial we'll cover everything from applying a sweat-proof base to keeping shine at bay and nailing the perfect disco-inspired eye.",
      subDescription:
        'Chat along with the MECCA community, ask us questions, and shop as you watch.',
      galleryImages: [
        {
          url:
            'https://res.cloudinary.com/mecca/image/upload/v1606100913/live-shopping/2020.11.19/dance-proof-beauty_wgb65o.jpg',
          alt: 'Dance Proof Beauty',
          title: 'Dance Proof Beauty',
          breakpoint: 'default',
        },
      ],
      eventCarouselImages: [
        {
          url:
            'https://res.cloudinary.com/mecca/image/upload/v1605231461/live-shopping/2020.11.19/dance-proof-450x600_udvb5w.jpg',
          alt: 'Dance Proof Beauty',
          title: 'Dance Proof Beauty',
          breakpoint: 'default',
        },
      ],
      bannerImages: [
        {
          url:
            'https://res.cloudinary.com/mecca/image/upload/t_mobile/v1605231792/live-shopping/2020.11.19/dance-proof-720x720_nkuwek.jpg',
          alt: 'Dance Proof Beauty',
          title: 'Dance Proof Beauty',
          breakpoint: 'default',
        },
        {
          url:
            'https://res.cloudinary.com/mecca/image/upload/t_tablet/v1605231792/live-shopping/2020.11.19/dance-proof-720x720_nkuwek.jpg',
          breakpoint: 'tablet',
        },
        {
          url:
            'https://res.cloudinary.com/mecca/image/upload/t_laptop/v1605231792/live-shopping/2020.11.19/dance-proof-720x720_nkuwek.jpg',
          breakpoint: 'laptop',
        },
        {
          url:
            'https://res.cloudinary.com/mecca/image/upload/t_desktop/v1605231792/live-shopping/2020.11.19/dance-proof-720x720_nkuwek.jpg',
          breakpoint: 'desktop',
        },
        {
          url:
            'https://res.cloudinary.com/mecca/image/upload/t_desktop_large/v1605231803/live-shopping/2020.11.19/dance-proof-1280x900_p4apeo.jpg',
          breakpoint: 'xl',
        },
      ],
    },
  ],
  banner: {
    title: 'MECCA Livestream',
    bgColor: '#fff6f4',
  },
  galleries: [
    {
      name: 'recent',
      order: 'DEFAULT',
      showIds: [],
      tagButtons: ['trending', 'makeup'],
    },
  ],
})
