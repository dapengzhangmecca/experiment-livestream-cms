import React, { useState } from 'react'
import { Input, Button } from 'antd'

import validateJSON from './validateJSON'
import demo from './demo'
import 'antd/dist/antd.css'

const JSONReader = ({ onChange = () => {} }) => {
  const [value, setValue] = useState('')
  const [errors, setErrors] = useState([])
  const handleSubmit = () => {
    try {
      const parsedJSON = JSON.parse(value)
      const validationResult = validateJSON(parsedJSON)
      if (validationResult.valid) {
        onChange(value)
        setErrors([])
      } else {
        const validationErrors = validationResult.errors.map(
          error => error.stack,
        )
        setErrors(validationErrors)
      }
    } catch (e) {
      setErrors([e.message])
    }
  }

  const handleDemo = () => {
    onChange(demo)
  }
  return (
    <>
      <h1>Existing JSON</h1>
      <Input.TextArea
        autoSize={{ minRows: 6, maxRows: 6 }}
        showCount
        onChange={event => {
          setValue(event.target.value)
        }}
      />

      {errors.map(error => (
        <div className="error" key={error}>
          {error}
        </div>
      ))}
      <Button type="primary" htmlType="button" onClick={handleSubmit}>
        Submit
      </Button>
      <Button type="primary" htmlType="button" onClick={handleDemo}>
        Demo
      </Button>
    </>
  )
}

export default JSONReader
