import { Validator } from 'jsonschema'

const responsiveImagesSchema = {
  id: '/responsiveImagesSchema',
  type: 'array',
  items: {
    properties: {
      url: { type: 'string' },
      alt: { type: 'string' },
      title: { type: 'string' },
      breakpoint: {
        enum: [
          'legacy',
          'mobile',
          'tablet',
          'tablet768',
          'laptop',
          'desktop',
          'smallDesktop',
          'xl',
          'xxl',
          'default',
        ],
      },
    },
    required: ['url'],
  },
}

const allShowsSchema = {
  id: '/allShowsSchema',
  type: 'array',
  items: {
    properties: {
      showId: { type: 'string' },
      tags: {
        type: 'array',
        items: { type: 'string' },
      },
      startTime: { type: 'string' },
      endTime: { type: 'string' },
      title: { type: 'string' },
      url: { type: 'string' },
      location: { type: 'string' },
      description: { type: 'string' },
      subDescription: { type: 'string' },
      bannerImages: { $ref: '/responsiveImagesSchema' },
      galleryImages: { $ref: '/responsiveImagesSchema' },
      eventCarouselImages: { $ref: '/responsiveImagesSchema' },
    },
    required: [
      'showId',
      'startTime',
      'endTime',
      'title',
      'url',
      'location',
      'description',
      'subDescription',
      'bannerImages',
      'galleryImages',
      'eventCarouselImages',
    ],
  },
}

const bannerSchema = {
  id: '/bannerSchema',
  type: 'object',
  properties: {
    showId: { type: 'string' },
    label: { type: 'string' },
    title: { type: 'string' },
    description: { type: 'string' },
    subDescription: { type: 'string' },
    textColor: { type: 'string' },
    bgColor: { type: 'string' },
    bannerThumbnails: { $ref: '/responsiveImagesSchema' },
    cta: {
      type: 'object',
      properties: {
        text: { type: 'string' },
        href: { type: 'string' },
      },
    },
    addToCalender: {
      type: 'object',
      properties: {
        start: { type: 'string' },
        end: { type: 'string' },
        title: { type: 'string' },
        description: { type: 'string' },
        location: { type: 'string' },
        url: { type: 'string' },
      },
    },
  },
  required: [],
}

const galleriesSchema = {
  id: '/galleriesSchema',
  type: 'array',
  items: {
    properties: {
      name: { type: 'string' },
      bgColor: { type: 'string' },
      order: {
        type: {
          enum: ['ASCENDING', 'DESCENDING', 'DEFAULT'],
        },
      },
      showIds: {
        type: 'array',
        items: { type: 'string' },
      },
      tagButtons: {
        type: 'array',
        items: { type: 'string' },
      },
    },
    required: ['showIds'],
  },
}

const schema = {
  type: 'object',
  properties: {
    allShows: { $ref: '/allShowsSchema' },
    banner: { $ref: '/bannerSchema' },
    galleries: { $ref: '/galleriesSchema' },
    required: [],
  },
}

const validator = new Validator()
validator.addSchema(responsiveImagesSchema, '/responsiveImagesSchema')
validator.addSchema(allShowsSchema, '/allShowsSchema')
validator.addSchema(bannerSchema, '/bannerSchema')
validator.addSchema(galleriesSchema, '/galleriesSchema')

export default object => validator.validate(object, schema)
