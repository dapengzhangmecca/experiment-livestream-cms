import React from 'react'
import { List, Collapse, Input, Form, Button } from 'antd'

const Item = ({ item = {}, handleSubmit }) => (
  <List.Item key={item.showId}>
    <Collapse>
      <Collapse.Panel header={`${item.title} - ${item.startTime}`}>
        <Form
          onFinish={handleSubmit}
          initialValues={{
            showId: item.showId,
            title: item.title,
            description: item.description,
            subDescription: item.subDescription,
            tags: JSON.stringify(item.tags),
            startTime: item.startTime,
            endTime: item.endTime,
            url: item.url,
            location: item.location,
            bannerImages: JSON.stringify(item.bannerImages),
            galleryImages: JSON.stringify(item.galleryImages),
            eventCarouselImages: JSON.stringify(item.eventCarouselImages),
          }}
        >
          <Form.Item name="showId">
            <Input addonBefore="showId" />
          </Form.Item>

          <Form.Item name="title">
            <Input addonBefore="title" />
          </Form.Item>

          <Form.Item name="description">
            <Input addonBefore="description" />
          </Form.Item>

          <Form.Item name="subDescription">
            <Input addonBefore="subDescription" />
          </Form.Item>

          <Form.Item name="tags">
            <Input addonBefore="tags" />
          </Form.Item>

          <Form.Item name="startTime">
            <Input addonBefore="startTime" />
          </Form.Item>

          <Form.Item name="endTime">
            <Input addonBefore="endTime" />
          </Form.Item>

          <Form.Item name="url">
            <Input addonBefore="url" />
          </Form.Item>

          <Form.Item name="location">
            <Input addonBefore="location" />
          </Form.Item>

          <Form.Item name="bannerImages">
            <Input addonBefore="bannerImages" />
          </Form.Item>

          <Form.Item name="galleryImages">
            <Input addonBefore="galleryImages" />
          </Form.Item>

          <Form.Item name="eventCarouselImages">
            <Input addonBefore="eventCarouselImages" />
          </Form.Item>

          <Form.Item>
            <Button type="primary" htmlType="submit">
              Submit
            </Button>
          </Form.Item>
        </Form>
      </Collapse.Panel>
    </Collapse>
  </List.Item>
)

export default Item
