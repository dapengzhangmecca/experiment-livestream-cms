import React from 'react'
import { List, Collapse } from 'antd'

import ListItem from './ListItem'
import 'antd/dist/antd.css'

const ShowList = ({ listData = [], handleUpdate }) => (
  <Collapse>
    <Collapse.Panel header="allShows">
      <List
        itemLayout="vertical"
        size="large"
        pagination={{
          pageSize: 5,
        }}
        dataSource={listData}
        renderItem={item => (
          <ListItem key={item.title} item={item} handleSubmit={handleUpdate} />
        )}
      />
    </Collapse.Panel>
  </Collapse>
)

export default ShowList
