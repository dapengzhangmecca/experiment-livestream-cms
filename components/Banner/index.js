import React from 'react'
import { Collapse, Form, Input, Select, Button } from 'antd'
import 'antd/dist/antd.css'

const Banner = ({ bannerConfig = {}, allShowIds = [], handleUpdate }) => (
  <Collapse>
    <Collapse.Panel header="banner">
      <Form
        onFinish={handleUpdate}
        initialValues={{
          showId: bannerConfig.showId,
          label: bannerConfig.label,
          title: bannerConfig.title,
          description: bannerConfig.description,
          subDescription: bannerConfig.subDescription,
          textColor: bannerConfig.textColor,
          bgColor: bannerConfig.bgColor,
          bannerThumbnails: JSON.stringify(bannerConfig.bannerThumbnails),
          cta: bannerConfig.cta,
          addToCalender: JSON.stringify(bannerConfig.addToCalender),
        }}
      >
        <Form.Item name="showId">
          <Select
            placeholder="Select a show ID"
            options={[
              { label: 'no show', value: '' },
              ...allShowIds.map(id => ({ label: id, value: id })),
            ]}
          />
        </Form.Item>

        <Form.Item name="label">
          <Input addonBefore="label" />
        </Form.Item>

        <Form.Item name="title">
          <Input addonBefore="title" />
        </Form.Item>

        <Form.Item name="description">
          <Input addonBefore="description" />
        </Form.Item>

        <Form.Item name="subDescription">
          <Input addonBefore="subDescription" />
        </Form.Item>

        <Form.Item name="textColor">
          <Input addonBefore="textColor" />
        </Form.Item>

        <Form.Item name="bgColor">
          <Input addonBefore="bgColor" />
        </Form.Item>

        <Form.Item name="bannerThumbnails">
          <Input addonBefore="bannerThumbnails" />
        </Form.Item>

        <Form.Item name="cta">
          <Input addonBefore="cta" />
        </Form.Item>

        <Form.Item name="addToCalender">
          <Input addonBefore="addToCalender" />
        </Form.Item>

        <Form.Item>
          <Button type="primary" htmlType="submit">
            Submit
          </Button>
        </Form.Item>
      </Form>
    </Collapse.Panel>
  </Collapse>
)

export default Banner
