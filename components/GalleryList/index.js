import React from 'react'
import { List, Collapse } from 'antd'

import ListItem from './ListItem'
import 'antd/dist/antd.css'

const GalleryList = ({ listData = [], allShowIds = [], handleUpdate }) => (
  <Collapse>
    <Collapse.Panel header="galleries">
      <List
        itemLayout="vertical"
        size="large"
        pagination={{
          pageSize: 5,
        }}
        dataSource={listData}
        renderItem={item => (
          <ListItem
            key={item.name}
            item={item}
            allShowIds={allShowIds}
            handleSubmit={handleUpdate}
          />
        )}
      />
    </Collapse.Panel>
  </Collapse>
)

export default GalleryList
