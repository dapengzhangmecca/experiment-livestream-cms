import React from 'react'
import { Collapse, Form, Input, Select, Button, Checkbox, List } from 'antd'
import 'antd/dist/antd.css'

const Item = ({ item = {}, allShowIds = [], handleSubmit = () => {} }) => (
  <List.Item key={item.name}>
    <Collapse>
      <Collapse.Panel header={item.name}>
        <Form
          onFinish={handleSubmit}
          initialValues={{
            name: item.name,
            bgColor: item.bgColor,
            order: item.order,
            showIds: item.showIds,
            tagButtons: JSON.stringify(item.tagButtons),
          }}
        >
          <Form.Item name="name">
            <Input addonBefore="name" />
          </Form.Item>

          <Form.Item name="bgColor">
            <Input addonBefore="bgColor" />
          </Form.Item>

          <Form.Item name="order">
            <Select
              placeholder="Select an order"
              options={[
                { label: 'ASCENDING', value: 'ASCENDING' },
                { label: 'DESCENDING', value: 'DESCENDING' },
                { label: 'DEFAULT', value: 'DEFAULT' },
              ]}
            />
          </Form.Item>

          <Form.Item name="showIds">
            <Checkbox.Group options={allShowIds} />
          </Form.Item>

          <Form.Item name="tagButtons">
            <Input addonBefore="tagButtons" />
          </Form.Item>

          <Form.Item>
            <Button type="primary" htmlType="submit">
              Submit
            </Button>
          </Form.Item>
        </Form>
      </Collapse.Panel>
    </Collapse>
  </List.Item>
)

export default Item
