import React from 'react'
import { useRouter } from 'next/router'
import { message } from 'antd'

import JSONReader from '../components/JSONReader'

const storeJson = async result => {
  const requestOptions = {
    method: 'POST',
    body: result,
    headers: new Headers({
      'Content-Type': 'application/json',
    }),
  }

  const response = await fetch('/api/json', requestOptions)
  if (response.status === 500) {
    throw new Error(response.json())
  }
}

export default function Input() {
  const router = useRouter()
  const handleChange = async result => {
    try {
      await storeJson(result)
      router.replace('/edit')
    } catch (error) {
      message.error(error)
    }
  }
  return <JSONReader onChange={handleChange} />
}
