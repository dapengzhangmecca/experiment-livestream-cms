// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
import { updateTypes } from '../../constant'

let json = {
  allShows: [],
  banner: {},
  galleries: [],
}

const handleUpdate = ({ type, value }) => {
  switch (type) {
    case updateTypes.allShowsItem: {
      const index = json.allShows.findIndex(
        item => item.showId === value.showId,
      )
      if (index === -1) {
        json.allShows.push(value)
      } else {
        json.allShows.splice(index, 1, value)
      }
      break
    }
    case updateTypes.banner: {
      json.banner = { ...json.banner, ...value }
      break
    }
    case updateTypes.galleries: {
      const index = json.galleries.findIndex(item => item.name === value.name)
      if (index === -1) {
        json.galleries.push(value)
      } else {
        json.galleries.splice(index, 1, value)
      }
      break
    }
    default: {
      break
    }
  }
}

export default (req, res) => {
  try {
    if (req.method === 'POST') {
      json = req.body
      res.statusCode = 200
      res.json({ message: 'success' })
    } else if (req.method === 'GET') {
      res.statusCode = 200
      res.json(json)
    } else if (req.method === 'PATCH') {
      const parsedRequestBody = JSON.parse(req.body)
      handleUpdate(parsedRequestBody)
      res.statusCode = 200
      res.json(json)
    } else {
      throw new Error('Invalid method')
    }
  } catch (error) {
    res.statusCode = 500
    res.json(error)
  }
}
