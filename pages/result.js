import React, { useEffect, useState } from 'react'
import { Button, Input } from 'antd'
import { useRouter } from 'next/router'
import 'antd/dist/antd.css'

const getJson = async () => {
  try {
    const response = await fetch('/api/json')
    const json = await response.json()
    console.log(json)
    return json
  } catch (error) {
    console.error(error)
    return null
  }
}

const Result = () => {
  const [data, setData] = useState({})
  const router = useRouter()
  const goBack = () => {
    router.replace('/edit')
  }

  const initialise = async () => {
    const json = await getJson()
    if (json) {
      setData(json)
    }
  }

  useEffect(() => {
    initialise()
  }, [])

  return (
    <div>
      <Input.TextArea
        autoSize={{ minRows: 10, maxRows: 10 }}
        showCount
        disabled
        value={JSON.stringify(data)}
      />
      <Button type="primary" htmlType="button" onClick={goBack}>
        Back
      </Button>
    </div>
  )
}

export default Result
