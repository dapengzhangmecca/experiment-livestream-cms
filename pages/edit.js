import React, { useEffect, useState } from 'react'
import { message, Button } from 'antd'
import { useRouter } from 'next/router'
import 'antd/dist/antd.css'

import ShowList from '../components/ShowList'
import Banner from '../components/Banner'
import GalleryList from '../components/GalleryList'
import { updateTypes } from '../constant'

const getJson = async () => {
  try {
    const response = await fetch('/api/json')
    const json = await response.json()
    return json
  } catch (error) {
    console.error(error)
    return null
  }
}

const Edit = () => {
  const [data, setData] = useState({})
  const router = useRouter()
  const goBack = () => {
    router.replace('/input')
  }
  const generateResult = () => {
    router.replace('/result')
  }

  const initialise = async () => {
    const initialData = await getJson()
    if (initialData) {
      setData(initialData)
    }
  }

  useEffect(() => {
    initialise()
  }, [])

  const handleUpdate = type => async values => {
    try {
      const body = JSON.stringify({
        type,
        value: values,
      })
      const requestOptions = {
        method: 'PATCH',
        body,
      }

      const response = await fetch('/api/json', requestOptions)
      const newJson = await response.json()
      setData(newJson)
      message.success('Success')
    } catch (e) {
      message.error(e)
    }
  }

  const allShowIds = data?.allShows?.map(show => show.showId)

  return (
    <div>
      <ShowList
        listData={data.allShows}
        handleUpdate={handleUpdate(updateTypes.allShowsItem)}
      />
      <Banner
        bannerConfig={data.banner}
        allShowIds={allShowIds}
        handleUpdate={handleUpdate(updateTypes.banner)}
      />
      <GalleryList
        listData={data.galleries}
        allShowIds={allShowIds}
        handleUpdate={handleUpdate(updateTypes.galleries)}
      />
      <Button type="primary" htmlType="button" onClick={goBack}>
        Back
      </Button>

      <Button type="primary" htmlType="button" onClick={generateResult}>
        Result
      </Button>
    </div>
  )
}

export default Edit
